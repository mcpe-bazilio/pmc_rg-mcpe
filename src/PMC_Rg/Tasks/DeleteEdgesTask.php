<?php


namespace PMC_Rg\Tasks;

use pocketmine\Player;
use pocketmine\scheduler\PluginTask;
use PMC_Rg\PMC_Rg;

class DeleteEdgesTask extends PluginTask {

	/** @var PMC_Rg $plugin */
	private $plugin;

	/** @var string $regionName */
	private $regionName;

	/** @var Player $sender */
	private $sender;

	public function __construct(PMC_Rg $Plugin, $sender, $regionName){
		parent::__construct($Plugin);
		$this->plugin = $Plugin;
		$this->regionName = $regionName;
		$this->sender = $sender;
	}

	public function onRun($arg){
		$this->plugin->deleteEdges($this->regionName, $this->sender);
	}
}
