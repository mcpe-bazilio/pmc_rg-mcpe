<?php

namespace PMC_Rg\Tasks;

use pocketmine\scheduler\PluginTask;
use PMC_Rg\PMC_Rg;

class DeleteEdgesRepeatedTask extends PluginTask {

	/** @var PMC_Rg $plugin */
	private $plugin;

	public function __construct(PMC_Rg $Plugin){
		parent::__construct($Plugin);
		$this->plugin = $Plugin;
	}

	public function onRun($tick){
		$eRegions = $this->plugin->edges->getAll();
		foreach($eRegions as $RegionName => $eRegion){
			if($eRegion['deleteTime'] < microtime(true)){
				$this->plugin->deleteEdges($RegionName);
			}
		}
	}
}
