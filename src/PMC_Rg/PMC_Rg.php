<?php
namespace PMC_Rg;

use pocketmine\block\Air;
use pocketmine\block\Block;
use pocketmine\command\Command as CMD;
use pocketmine\command\CommandSender as CS;
use pocketmine\event\Listener as L;
use pocketmine\level\Position;
use pocketmine\Player;
use pocketmine\plugin\PluginBase as PB;
use pocketmine\utils\Config;
use pocketmine\utils\TextFormat as F;

class PMC_Rg extends PB implements L {

	const EDGE_BLOCK_ID_1 = Block::BEACON;
	const EDGE_BLOCK_ID_2 = Block::OBSIDIAN;

	public $pos1 = [];
	public $pos2 = [];
	public $scheduledEdgesTasks = [];
	public $subCommands = [];
	public $subCommandAliases = [];

	/** @var Config $areas */
	public $areas;

	/*** @var Config $edges */
	public $edges;

	/*** @var Config $config */
	private $config;

	public $flagValues = [
		'0'      => 0,
		'none'   => 0,
		'1'      => 1,
		'owner'  => 1,
		'2'      => 2,
		'member' => 2,
		'3'      => 3,
		'all'    => 3
	];


	public $flagColors = ['§7', '§c', '§6', '§a'];
	public $flagValuesTxt = ['запрещено всем', 'разрешено собственникам', 'разрешено собственникам и жильцам', 'разрешено всем'];


	function onEnable(){
		@mkdir($this->getDataFolder());
		$this->getServer()->getPluginManager()->registerEvents(new EventListener($this), $this);

		$this->areas = new Config($this->getDataFolder() . "Areas.yml", Config::YAML);
		$this->areas->save();

		$this->edges = new Config($this->getDataFolder() . "Edges.yml", Config::YAML);
		$this->edges->save();

		$this->subCommands = $this->getDescription()->getCommands()['rg']['subCommands'];
		foreach($this->subCommands as $subCmdName => $subCmdInfo){
			if(isset($subCmdInfo['aliases'])){
				foreach($subCmdInfo['aliases'] as $alias)
					$this->subCommandAliases[$alias] = $subCmdName;
			}
		}

		$pFlagList = $this->subCommands['flag']["flagList"];
		$DefaultFlags = [];
		foreach($pFlagList as $flag => $flagInfo){
			$DefaultFlags[$flag] = $flagInfo['default'];
		}
		$DefaultSettings = [
			"MaxAreas"     => 5,
			"MaxAreaSize"  => 25000,
			"DefaultFlags" => $DefaultFlags
		];
		$this->config = new Config($this->getDataFolder() . "Settings.yml", Config::YAML, $DefaultSettings);

		//Удаляем несуществующие флаги
		$cfg = $this->config->getAll();
		foreach($cfg["DefaultFlags"] as $flag => $value){
			if(!array_key_exists($flag, $pFlagList)){
				unset($cfg["DefaultFlags"][$flag]);
			}
		}
		$this->config->setAll($cfg);
		$this->config->save();

		$this->getServer()->getScheduler()->scheduleDelayedRepeatingTask(new Tasks\DeleteEdgesRepeatedTask($this), 20 * 60, 20 * 60);

		$this->getLogger()->info(F::GREEN . "Приват система включена!");
	}


	public function onCommand(CS $s, CMD $cmd, $label, array $args){

		if($cmd->getName() != "rg") return false;

		if(!($s instanceof Player)) return $this->MSG($s, 0, "[✘] Эта команда может использоваться только в игре!");

		if(!$s->hasPermission("rg.p")) return $this->MSG($s, 0, "[✘] У Вас недостаточно прав.");

		//Убираем пустые ргументы
		$args2 = [];
		foreach($args as $key => $arg){
			if(trim($arg) != '') $args2[] = $arg;
		}
		$args = $args2;

		if(!isset($args[0])){
			return $this->printHelp($s);
		}

		$subCmd = $subCmdOrig = strtolower($args[0]);
		$pSubCommands = $this->subCommands;

		$unknownCmdMsg = "[✘] Неизвестная подкоманда " . $this->ec($subCmd, 0) . " " . $this->getCmdUsage('help', 0, true) . " для справки.";

		if(!array_key_exists($subCmd, $pSubCommands)){
			if(!array_key_exists($subCmd, $this->subCommandAliases)){
				return $this->MSG($s, 0, $unknownCmdMsg);
			}
			$subCmd = $this->subCommandAliases[$subCmd];
		}

		if(!$this->canUseRGCommand($s, $subCmd)){
			return $this->MSG($s, 0, "[✘] У Вас нет прав на выполнение команды " . $this->frc("/rg $subCmdOrig", 0));
		}


		$pName = $s->getName();
		$player = $s->getServer()->getPlayer($pName);
		$pNameLC = strtolower($pName);
		$currentLevelName = $player->getLevel()->getName();


		$pX = $s->getFloorX();
		$pY = $s->getFloorY();
		$pZ = $s->getFloorZ();

		$pFlag = $pSubCommands['flag'];
		$pFlagList = $pFlag["flagList"];


		switch($subCmd){

			case "pos1":
			case "pos2":
				$thisPos = $subCmd;
				if($subCmd == "pos1"){
					$pn = 1;
					$OtherPos = "pos2";
				}else{
					$pn = 2;
					$OtherPos = "pos1";
				}

				$this->$thisPos[$pNameLC] = [0 => $pX, 1 => ($pY - 1), 2 => $pZ, 'level' => $currentLevelName];
				$p = $this->$thisPos[$pNameLC];

				foreach($this->areas->getAll() as $regionName => $region){
					if($currentLevelName == $region["level"]){
						if($this->inRegion($region, $p[0], $p[1], $p[2])){
							unset($this->$thisPos[$pNameLC]);
							$msg = "[✘] Здесь размечать новый участок нельзя - приватная территория!\n";
							$msg .= $this->getCmdUsage('info', 0, true) . ' для проверки занятости территории.';
							return $this->MSG($s, 0, $msg);
						}
					}
				}

				$msg = "[✔] Установлена " . $this->ec($pn . '-я', 1, '§b') . " точка участка. " . $this->fCrd($p[0], $p[1], $p[2], true);

				if(isset($this->$OtherPos[$pNameLC])){
					$pp = $this->$OtherPos[$pNameLC];
					foreach($this->areas->getAll() as $regionName => $region){
						if($currentLevelName == $region["level"]){
							if($this->inRegion($region, $pp[0], $pp[1], $pp[2])){
								unset($this->$OtherPos[$pNameLC]);
								$msg .= "\n§d[✘] Точка $OtherPos оказалась на чужой территории и была удалена.\n";
								$msg .= "\nЗадайте снова " . ($pn == 1 ? 'вторую' : 'первую') . " точку командой " . $this->getCmdUsage($OtherPos, 1);
								return $this->MSG($s, 1, $msg);
							}

							if($this->intersectWithRegion($region, $p[0], $p[1], $p[2], $pp[0], $pp[1], $pp[2])){
								unset($this->$thisPos[$pNameLC]);
								$msg = "[✘] Новый участок будет пересекаться с другим участком. Попробуйте поставить точку в другом месте.\n";
								$msg .= $this->getCmdUsage('info', 0, true) . ' для проверки занятости территории.';
								return $this->MSG($s, 0, $msg);
							}
						}
					}
					$msg .= ".§a Объем участка: " . $this->ec($this->getVolumePP($pNameLC), 1, '§d') . " блоков. "
						. "\nДалее создайте этот участок: " . $this->getCmdUsage('create', 1);
				}else{
					$msg .= "\nДалее задайте в нужном месте " . ($pn == 1 ? 'вторую' : 'первую') . " точку командой " . $this->getCmdUsage($OtherPos, 1);

				}
				return $this->MSG($s, 1, $msg);

			case "expand":

				if(!isset($args[1]) || !isset($args[2])){
					return $this->MSG($s, 0, "[✘] " . $this->getCmdUsage($subCmd, 0, true));
				}

				$direction = strtolower($args[1]);
				if($direction != "up" && $direction != "down"){
					return $this->MSG($s, 0, "[✘] " . $this->getCmdUsage($subCmd, 0, true));
				}

				$quantity = $args[2];
				if(!is_numeric($quantity)){
					return $this->MSG($s, 0, "[✘] Укажите корректное количество блоков цифрами.");
				}

				if(!isset($this->pos1[$pNameLC]) || !isset($this->pos2[$pNameLC])){
					return $this->MSG($s, 0, "[✘] Для начала выделите крайние точки территории командами "
						. $this->getCmdUsage('pos1', 1) . " §cи " . $this->getCmdUsage('pos2', 1) . ".");
				}

				$tmp = '4520974092756g7d5nsur22m0!733';
				$this->pos1[$pNameLC . $tmp] = $this->pos1[$pNameLC];
				$this->pos2[$pNameLC . $tmp] = $this->pos2[$pNameLC];


				if($this->pos1[$pNameLC][1] < $this->pos2[$pNameLC][1]){
					$yMin = &$this->pos1[$pNameLC][1];
					$yMax = &$this->pos2[$pNameLC][1];
				}else{
					$yMin = &$this->pos2[$pNameLC][1];
					$yMax = &$this->pos1[$pNameLC][1];
				}

				if($direction == "up"){
					$yMax += $quantity;
					$rusDir = 'вверх';
				}else{
					$yMin -= $quantity;
					$rusDir = 'вниз';
				}
				$p = $this->pos1[$pNameLC];
				$pp = $this->pos2[$pNameLC];

				foreach($this->areas->getAll() as $regionName => $region){
					if($currentLevelName == $region["level"]){
						if($this->intersectWithRegion($region, $p[0], $p[1], $p[2], $pp[0], $pp[1], $pp[2])){
							$this->pos1[$pNameLC] = $this->pos1[$pNameLC . $tmp];
							$this->pos2[$pNameLC] = $this->pos2[$pNameLC . $tmp];
							unset($this->pos1[$pNameLC . $tmp]);
							unset($this->pos2[$pNameLC . $tmp]);
							$msg = "[✘] Раширение границ новго участка $rusDir отменено: расширенный участок будет пересекаться с другим участком. \n";
							$msg .= $this->getCmdUsage('info', 0, true) . ' для проверки занятости территории.';
							return $this->MSG($s, 0, $msg);
						}
					}
				}

				$msg = "[✔] Выделение участка расширено на " . $this->ec($quantity, 1) . " блоков $rusDir.";
				$msg .= "\n§aИнформация о будущем участке: " . $this->getCmdUsage('gps', 1);
				return $this->MSG($s, 1, $msg);

			case "gps":
				$bPos1 = false;
				$bPos2 = false;
				$info = 'Ваши координаты: ' . $this->fCrd($pX, $pY, $pZ) . '.§e Точки нового участка: ';
				$infoPos = '';
				if(isset($this->pos1[$pNameLC])){
					$bPos1 = true;
					$p1 = $this->pos1[$pNameLC];
					$infoPos .= "§b pos1: " . $this->fCrd($p1[0], $p1[1], $p1[2]) . " ";
				}
				if(isset($this->pos2[$pNameLC])){
					$bPos2 = true;
					$p2 = $this->pos2[$pNameLC];
					$infoPos .= ($info == '' ? '' : ',') . "§f pos2: " . $this->fCrd($p2[0], $p2[1], $p2[2]) . " ";
				}

				$info .= ($bPos1 || $bPos2 ? $infoPos : 'не заданы.');
				if($bPos1 && $bPos2){
					$info .= ".§e Объем участка: " . $this->ec($this->getVolumePP($pNameLC), 3, '§d') . " блоков.";
				}
				return $this->MSG($s, 3, $info);

			case "create":
				if(!isset($this->pos1[$pNameLC]) && !isset($this->pos2[$pNameLC])){
					return $this->MSG($s, 0, '[✘] Сначала необходимо выбрать крайние точки для территории командами '
						. $this->getCmdUsage('pos1', 0) . ' и ' . $this->getCmdUsage('pos2', 0));
				}elseif(!isset($this->pos1[$pNameLC])){
					return $this->MSG($s, 0, '[✘] Сначала задайе первую точку для территории командой ' . $this->getCmdUsage('pos1', 0));
				}elseif(!isset($this->pos2[$pNameLC])){
					return $this->MSG($s, 0, '[✘] Сначала задайе вторую точку для территории командой ' . $this->getCmdUsage('pos2', 0));
				}

				if(!isset($args[1])){
					return $this->MSG($s, 0, "[✘] Вы не указали название участка. " . $this->getCmdUsage($subCmd, 0, true));
				}

				if($this->areas->exists(strtolower($args[1]))){
					return $this->MSG($s, 0, "[✘] Участок с таким названием уже существует.");
				}

				$pos1 = $this->pos1[$pNameLC];
				$pos2 = $this->pos2[$pNameLC];

				if($pos1["level"] != $pos2["level"]){
					return $this->MSG($s, 0, "[✘] Точки участка должны находиться в одном мире.");
				}

				$minX = min($pos1[0], $pos2[0]);
				$maxX = max($pos1[0], $pos2[0]);
				$minY = min($pos1[1], $pos2[1]);
				$maxY = max($pos1[1], $pos2[1]);
				$minZ = min($pos1[2], $pos2[2]);
				$maxZ = max($pos1[2], $pos2[2]);

				$volume = $this->getVolumePP($pNameLC);

				$cfg = $this->config->getAll();
				if($volume > $cfg["MaxAreaSize"] && !$s->hasPermission("rg.do-all") && !$s->hasPermission("rg.max-area-size")){
					return $this->MSG($s, 0, "[✘] Максимальный размер территории - " . $this->ec($cfg["MaxAreaSize"], 0) . " блоков. У Вас - " . $this->ec($volume, 0) . " блоков.");
				}

				if(count($this->areas->getAll()) != 0){
					$playerAreas = 0;
					foreach($this->areas->getAll() as $regionName => $region){
						if(in_array($pNameLC, $region["owners"])){
							$playerAreas++;
						}
					}

					if($playerAreas > $cfg["MaxAreas"] && !$s->hasPermission("rg.do-all") && !$s->hasPermission("rg.max-areas")){
						return $this->MSG($s, 0, "[✘] У Вас уже есть максимальное количество участков (" . $cfg["MaxAreas"] . ").");
					}

					foreach($this->areas->getAll() as $regionName => $region){
						if($currentLevelName == $region["level"]){
							if($this->intersectWithRegion($region, $minX, $minY, $minZ, $maxX, $maxY, $maxZ)){
								return $this->MSG($s, 0, "[✘] Выбранная территория задевает чужие участки. Задайте заново точки pos1 и pos2.");
							}
						}
					}
				}
				$this->areas->set(strtolower($args[1]), [
					"x"       => [$minX, $maxX],
					"y"       => [$minY, $maxY],
					"z"       => [$minZ, $maxZ],
					"owners"  => [$pNameLC],
					"members" => [],
					"level"   => $pos1["level"],
					"flags"   => $cfg["DefaultFlags"]
				]);
				$this->areas->save();

				$this->MSG($s, 1, "[✔] Участок " . $this->frn($args[1], 1) . " создан! Объем участка: " . $this->ec($volume, 1, '§d') . " блоков.");

				$this->getLogger()->info(F::AQUA . $pName . F::YELLOW . " создал приватный участок " . $this->frn($args[1], 3)
					. " в мире " . $this->frw($currentLevelName, 3));
				unset($this->pos1[$pNameLC]);
				unset($this->pos2[$pNameLC]);
				return true;
			case "delete":
				if(!isset($args[1])){
					return $this->MSG($s, 0, "[✘] " . $this->getCmdUsage($subCmd, 0, true));
				}

				$regionName = $args[1];
				$regionNameLC = strtolower($regionName);

				if(!$this->areas->exists($regionNameLC)){
					return $this->MSG($s, 0, "[✘] Участок " . $this->frn($regionName, 0) . " не существует.");
				}
				$region = $this->areas->get($regionNameLC);

				if(!in_array($pNameLC, $region["owners"]) && !$s->hasPermission("rg.do-all")){
					return $this->MSG($s, 0, "[✘] У Вас нет прав для удаления этого участка.");
				}
				$this->deleteEdges($regionName, $s);
				$this->areas->remove($regionNameLC);
				$this->areas->save();
				return $this->MSG($s, 1, "[✔] Участок " . $this->frn($regionName, 1) . " удален!");

			case "addmember":
				return $this->updateGroup($s, $args, "members", "add");
			case "deletemember":
				return $this->updateGroup($s, $args, "members", "delete");
			case "addowner":
				return $this->updateGroup($s, $args, "owners", "add");
			case "deleteowner":
				return $this->updateGroup($s, $args, "owners", "delete");

			case "info":

				$region = null;
				if(isset($args[1])){
					$regionName = $args[1];
					$regionNameLC = strtolower($regionName);

					if(!$this->areas->exists($regionNameLC)){
						return $this->MSG($s, 0, "[✘] Участок " . $this->frn($regionName, 0) . " не существует.");
					}
					$region = $this->areas->get($regionNameLC);

					if(!$s->hasPermission("rg.do-all") && !$this->checkFlagForRegion($pNameLC, $region, 'info')){
						return $this->MSG($s, 3, "[✘] У вас нет прав на просмотр информации об участке " . $this->frn($regionName, 0));
					}
					$msg = "[✔] ======== Информация об участке " . $this->frn($regionName, 3) . " =============";
				}else{
					//TODO Проверить
					$arIn = $this->getRegionInOver($pX, $pY, $pZ);

					if($arIn[0] === 0) return $this->MSG($s, 3, "[✔] Там, где вы находитесь нет приватных участков. Территория свободна!");
					$regionName = $arIn[1];
					$region = $arIn[2];
					if(!$s->hasPermission("rg.do-all") && !$this->checkFlagForRegion($pNameLC, $region, 'info')){
						return $this->MSG($s, 1, "[✔] Вы находитесь " . $arIn[3] . ". Но у вас нет прав на просмотр информации о нем.");
					}
					$msg = "[✔] ===== Вы находитесь " . $arIn[3] . " " . $this->frn($regionName, 3) . ". Об участке: ";
				}

				$px = $region["x"];
				$py = $region["y"];
				$pz = $region["z"];
				$msg .= ("\nМир: " . $this->frw($region["level"], 3) .
					" §fГраницы:§b min: " . $this->fCrd($px[0], $py[0], $pz[0]) . " §b max: " . $this->fCrd($px[1], $py[1], $pz[1]) .
					'§f, объем: ' . $this->ec($this->getVolume($region), 3, '§b')
				);

				$msg .= ("\nВладельцы: §d" . implode(" , ", $region["owners"]));
				if(count($region["members"]) == 0){
					$members = "§8нет";
				}else{
					$members = "§d" . implode(" , ", $region["members"]);
				}
				$msg .= ("\nЖильцы: " . $members);

				$dlm = '§7,';
				$flags = '';
				foreach($region["flags"] as $flagName => $flagValue){
					if(!$this->checkChangeFlagRights($s, $region, $flagName)) continue;
					$flags .= $this->flagColors[$this->flagValues[$flagValue]] . ' ' . $flagName . $dlm;
				}
				$flags = $this->s_rtrim($flags, $dlm);
				$msg .= ("\nФлаги, которые можете менять: " . ($flags == '' ? '§8нет.' : ("\n" . $flags)));

				$flags = '';
				foreach($region["flags"] as $flagName => $flagValue){
					if($this->checkChangeFlagRights($s, $region, $flagName)) continue;
					$flags .= $this->flagColors[$this->flagValues[$flagValue]] . ' ' . $flagName . $dlm;
				}
				$flags = $this->s_rtrim($flags, $dlm);
				$msg .= ("\nФлаги, которые НЕ можете менять: " . ($flags == '' ? '§8нет.' : ("\n" . $flags)));

				$msg .= ("\n(цвета разрешений:" . $this->flagColors[0] . ' никому§8,' . $this->flagColors[1]
					. ' собственникам§8,' . $this->flagColors[2] . ' собственникам и жителям§8,' . $this->flagColors[3] . ' всем§e)');
				$msg .= "\n" . $this->getFlagHelpUsage();

				return $this->MSG($s, 3, $msg);

			case "list":

				//Если второй аргумент не указан, отображаем участки игрока, выполнившего команду
				//Если второй аргумент указан, то это имя игрока и отображаем его участки
				if(isset($args[1])){
					$pNameLC_q = strtolower($args[1]);
					if($pNameLC_q != $pNameLC && !$s->hasPermission("rg.do-all") && !$s->hasPermission("rg.command.list.nick")){
						return $this->MSG($s, 0, "[✘] У Вас недостаточно прав на просмотр списка участков другогих игроков.");
					}
					$bMy = false;
				}else{
					$pNameLC_q = $pNameLC;
					$bMy = true;
				}

				$regionsList = [];
				foreach($this->areas->getAll() as $regionName => $region){
					if(in_array($pNameLC_q, $region["owners"])) $regionsList[$regionName] = $region;
				}

				if(count($regionsList) == 0) return $this->MSG($s, 3, "[✘] У " . ($bMy ? 'вас' : 'игрока ' . $this->ec($args[1], 3)) . " нет участков.");

				$msg = "[✔] " . ($bMy ? 'Ваши участки' : 'Участки игрока ' . $this->ec($args[1], 3)) . " :";
				foreach($regionsList as $regionName => $region){
					$px = $region["x"];
					$py = $region["y"];
					$pz = $region["z"];
					$msg .= ("\n" . $this->frn($regionName, 3) . ' - мир: ' . $this->frw($region["level"], 3) .
						',  координаты: §b min: ' . $this->fCrd($px[0], $py[0], $pz[0]) . ' §b max: ' . $this->fCrd($px[1], $py[1], $pz[1])
						. '§e, объем: ' . $this->ec($this->getVolume($region), 3, '§b') . ' '
					);
				}
				return $this->MSG($s, 3, $msg);

			case "flaginfo":
				if(isset($args[1])){
					// /rg flaginfo ФЛАГ
					$flagName = strtolower($args[1]);
					if(array_key_exists($flagName, $pFlagList)){
						$msg = "Флаг §b$flagName §e: " . $pFlagList[$flagName]['descr'];
						if(isset($pFlagList[$flagName]['descrAdd'])) $msg .= '. ' . $pFlagList[$flagName]['descrAdd'];
						return $this->MSG($s, 3, $msg);
					}else{
						return $this->MSG($s, 0, "[✘] Флаг " . $this->ec($flagName, 0) . ' не существует.');
					}
				}else{
					// /rg flaginfo
					$hlp = $pFlag['about']
						. "\n" . $pFlag['flagValues']
						. "\n=========== Списк флагов ==========";
					foreach($pFlagList as $flagName => $flagInfo){
						$hlp .= "\n§b$flagName §e: " . $flagInfo['descr'];
					}
					$hlp .= "\n" . $this->getFlagHelpUsage();
					return $this->MSG($s, 3, $hlp);
				}

			case "flag":

				$availableFlagsList = "Флаги участков: §b";
				foreach($pFlagList as $f => $descr){
					$availableFlagsList .= "$f, ";
				}
				$availableFlagsList = $this->s_rtrim($availableFlagsList, ', ');

				$aboutFlagHelp = "\n§e" . $availableFlagsList . "\n" . $this->getFlagHelpUsage();

				if(!isset($args[1]) || !isset($args[2])){
					return $this->MSG($s, 0, "[✘] Установка значения флага: " . $this->getCmdUsage($subCmd, 0) . " " . $aboutFlagHelp);
				}

				$regionName = $args[1];
				$regionNameLC = strtolower($regionName);

				if(!$this->areas->exists($regionNameLC)){
					return $this->MSG($s, 0, "[✘] Участок " . $this->frn($regionName, 0) . " не существует.");
				}

				$flagName = strtolower($args[2]);

				if(!array_key_exists($flagName, $pFlagList)){
					return $this->MSG($s, 0, "[✘] Флаг " . $this->ec($flagName, 0) . " не существует." . $aboutFlagHelp);
				}

				$region = $this->areas->get($regionNameLC);

				if(!isset($args[3])){
					// Значение флага не указано, значит выдаем его значение
					if(!$s->hasPermission("rg.do-all") && !$this->checkFlagForRegion($pNameLC, $region, 'info')){
						return $this->MSG($s, 0, "[✘] У вас нет прав на просмотр значений флагов на участке " . $this->frn($regionName, 0));
					}
					$flagValue = (isset($region["flags"][$flagName]) ? $region["flags"][$flagName] : 0);
					$fValTxt = $this->flagValuesTxt[$flagValue];
					$msg = "[✔] На участке " . $this->frn($regionName, 1) . " флаг " . $this->ec($flagName, 1) . "§7 = " . $this->flagColors[$flagValue] . "$flagValue  ($fValTxt)";
					$msg .= "\n" . $this->getFlagHelpUsage($flagName, '§7');

					return $this->MSG($s, 1, $msg);
				}

				//Иначе, если значение флага задано, пытаемся его установить

				if(!$this->checkChangeFlagRights($s, $region, $flagName)){
					return $this->MSG($s, 0, "[✘] У Вас нет права изменение флага " . $this->ec($flagName, 0) . " на участке " . $this->frn($regionName, 0) . "");
				}

				$permission = strtolower($args[3]);
				if(!isset($this->flagValues[$permission])){
					return $this->MSG($s, 0, "[✘] Вы неверно указали значение " . $this->ec($args[3], 0)
						. " для флага " . $this->ec($flagName, 0) . "\n" . $pFlag['flagValues']);
				}

				return $this->MSG($s, 1, $this->SetFlag($flagName, $this->flagValues[$permission], $regionName));

			case "help":
				return $this->printHelp($s);

			case "tp":
				if(!isset($args[1])) return $this->MSG($s, 0, "[✘] " . $this->getCmdUsage($subCmd, 0, true));
				$regionName = $args[1];
				$regionNameLC = strtolower($regionName);
				if(!$this->areas->exists($regionNameLC)) return $this->MSG($s, 0, "[✘] Участок " . $this->frn($regionName, 0) . " не существует.");
				$region = $this->areas->get($regionNameLC);

				if(!$this->checkFlagForRegion($pNameLC, $region, 'entry')){
					return $this->MSG($s, 0, "[✘] У Вас нет прав на телепортацию к участку " . $this->frn($regionName, 0));
				}

				$px = $region["x"];
				$pz = $region["z"];
				if(isset($args[2])){
					if(2 == (int)$args[2]){
						$x = (int)$px[1] - 1;
						$z = (int)$pz[1] - 1;
					}else{
						$x = (int)$px[0] + 1;
						$z = (int)$pz[0] + 1;
					}
				}else{
					$x = (int)$px[0] + floor(((int)$px[1] - (int)$px[0]) / 2);
					$z = (int)$pz[0] + floor(((int)$pz[1] - (int)$pz[0]) / 2);
				}

				$levelName = $region["level"];
				$level = $this->getServer()->getLevelByName($levelName);
				if($level == null) return $this->MSG($s, 0, '[✘] Не найден мир ' . $this->frw($levelName, 0)
					. 'в котором расположен участок ' . $this->frn($regionName, 0));

				$y = $level->getHighestBlockAt($x, $z);
				$pos = new Position($x, $y, $z, $level);
				if($player->teleport($pos)){
					$this->MSG($s, 1, "[✔] Вы перемещаетесь к участку " . $this->frn($regionName, 1) . " в точку: " . $this->fCrd($x, $y, $z));
				}
				return true;

			case "tpp":
				if(!isset($args[1]) || !isset($args[2])) return $this->MSG($s, 0, "[✘] " . $this->getCmdUsage($subCmd, 0, true));
				if((intval($args[1]) . '') != $args[1] || (intval($args[2]) . '') != $args[2]) return $this->MSG($s, 0, "[✘] Координаты X и Z должны быть целыми числами.");
				if(!$s->hasPermission("rg.do-all") && !$s->hasPermission("rg.command.tpp")) return $this->MSG($s, 0, "[✘] У Вас нет прав на телепортацию.");

				$level = $this->getServer()->getLevelByName($currentLevelName);
				if($level == null) return $this->MSG($s, 0, '[✘] Не найден мир ' . $this->frw($currentLevelName, 0));
				$x = (int)$args[1];
				$z = (int)$args[2];
				$y = $level->getHighestBlockAt($x, $z) + 1;
				$pos = new Position($x, $y, $z, $level);
				$result = $player->teleport($pos);
				if($result){
					$this->MSG($s, 1, "[✔] Вы перемещаетесь в: " . $this->fCrd($x, $y, $z));
				}
				return true;

			case "reloadareas":
				$this->reloadAndRepairAreas();
				return $this->MSG($s, 1, "[✔] areas.yml перезагружен");

			case "showedges":
			case "hideedges":
				$region = null;
				if(isset($args[1])){
					$regionName = $args[1];
					$regionNameLC = strtolower($regionName);
					if(!$this->areas->exists($regionNameLC)){
						return $this->MSG($s, 0, "[✘] Участок " . $this->frn($regionName, 0) . " не существует.");
					}
					$region = $this->areas->get($regionNameLC);
					if(!$s->hasPermission("rg.do-all") && !in_array($pNameLC, $region["owners"])){
						return $this->MSG($s, 0, "[✘] Право на отображение/скрытие границ участка есть только у собственника.");
					}
				}else{
					$arIn = $this->getRegionInOver($pX, $pY, $pZ);

					if($arIn[0] === 0) return $this->MSG($s, 0, "[✘] В этом месте нет приватных участков.");
					$regionName = $arIn[1];
					$region = $arIn[2];
					if(!$s->hasPermission("rg.do-all") && !in_array($pNameLC, $region["owners"])){
						return $this->MSG($s, 1, "[✔] Вы находитесь " . $arIn[3] . ". Но у вас нет прав на отображение/скрытие его границ.");
					}

				}

				$regionNameLC = strtolower($regionName);
				$levelName = $region["level"];
				$level = $this->getServer()->getLevelByName($levelName);
				if($level == null){
					return $this->MSG($s, 0, '[✘] Не найден мир ' . $this->frw($levelName, 0) . 'в котором расположен участок ' . $this->frn($regionName, 0));
				}
				switch($subCmd){
					case "showedges":
						$timeToShowSec = 15;
						if(isset($args[2])){
							$timeToShowSec = intval($args[2]);
						}
						if(!$s->hasPermission("rg.do-all")){
							$timeToShowSec = min(300, $timeToShowSec);
						}
						$this->setRegionEdges($regionName, $s, $timeToShowSec);
						if(isset($args[2]) && $args[2] == '!'){
							$this->cancelScheduledEdgesTasks($regionNameLC);
						}
						return true;
					case "hideedges":
						if(isset($args[2]) && 'f' == $args[2]){
							$this->forceDeleteRegionEdges($regionName, $s);
						}else{
							$this->deleteEdges($regionName, $s);
						}
						return true;
				}
				return true;
			default:
				return $this->MSG($s, 0, $unknownCmdMsg);
		}
	}

	public function MSG(CS $s, $ResultCode, $msg, $success = true){
		$color = $this->getColorByResultCode($ResultCode);
		$s->sendMessage($color . $msg);
		return $success;
	}

	private function getColorByResultCode($ResultCode){
		switch($ResultCode){
			case 0:
				return F::RED;
			case 1:
				return F::GREEN;
			case 2:
				return F::AQUA;
			case 3:
				return F::YELLOW;
		}
		return F::AQUA;
	}

	public function printHelp(CS $s, $success = true){
		$hlp = "§f======§d  rg help  §f====== §1(плагин разработан специально для §5PLAY-MC.RU§1) §f======";
		foreach($this->subCommands as $subCmdName => $sc){
			if(isset($sc["no-help"]) && $sc["no-help"]) continue;
			$usage = $this->getCmdUsage($subCmdName, 3);
			$descr = isset($sc["description"]) ? $sc["description"] : '';
			$hlp .= "\n§e" . $usage . '§a - ' . $descr;
		}
		$hlp .= "\n§7Выделенные §bбуквы§7 в командах можно использовать как алиасы. Например, §e/rg §bse§7 вместо §e/rg §bs§ehow§be§edges §7";
		$this->MSG($s, 3, $hlp);
		return $success;
	}

	public function getCmdUsage($subCmdName, $ResultCode, $bLabel = false, $bAlias = true){
		if(!isset($this->subCommands[$subCmdName])) return '';
		$sc = $this->subCommands[$subCmdName];
		if(!isset($sc["usage"])) return '';

		$usageColor = '§e';
		$aliasColor = '§b';

		$prefix = ($bLabel ? 'Используйте: ' : '') . $usageColor;
		$usage = $sc["usage"];
		$aliasSuffix = '';
		$endColor = $this->getColorByResultCode($ResultCode);

		if($bAlias){
			if(isset($sc['aliases']) && isset($sc['aliases'][0])){
				$alias = $sc['aliases'][0];
				if(!$this->colorAlias($usage, $alias, $aliasColor, $usageColor)){
					$aliasSuffix = '§f Алиас:' . $usageColor . ' /rg' . $aliasColor . ' ' . $alias;
				}
			}
		}
		return $prefix . $usage . $aliasSuffix . $endColor;
	}


	function colorAlias(&$usage, $alias, $aliasColor = '§b', $usageColor = '§e'){
		if(!preg_match('%^([a-z0-9§]*/rg\s+)([a-z0-9§]+)(.*)$%im', $usage, $matches)) return false;
		$subCmdName = $result = preg_replace('/§\w/i', '', $matches[2]);
		if(preg_match('/^(\w*?)(' . $alias . ')(\w*)$/im', $subCmdName, $m)){
			$subCmdName = $m[1] . $aliasColor . $m[2] . $usageColor . $m[3];
			$usage = $usageColor . $matches[1] . $subCmdName . $matches[3];
			return true;
		}

		$usageNew = $usageColor . $matches[1];
		$residue = $matches[3];
		$aliasLetters = str_split($alias);

		foreach($aliasLetters as $letter){
			if(!preg_match('/^(\w*?)' . $letter . '(\w*)/i', $subCmdName, $matches)) return false;
			$usageNew .= $matches[1] . $aliasColor . $letter . $usageColor;
			$subCmdName = $matches[2];
		}
		$usage = $usageNew . $matches[2] . $residue;
		return true;
	}


	public function getRegionInOver($x, $y, $z){
		$res = [0, null, null]; // Флаг "внутри", название участка, участок
		foreach($this->areas->getAll() as $regionName => $region){
			$bIn = $this->overInRegion($region, $x, $y, $z);
			if($bIn > 0){
				if($bIn <= $res[0]) continue;
				$res[0] = $bIn;
				$res[1] = $regionName;
				$res[2] = $region;
				if($bIn == 2) break;
			}
		}
		$res[3] = ($res[0] == 3 ? 'на приватном участке' : ($res[0] == 2 ? 'над приватным участком' : ($res[0] == 1 ? 'под приватным участком' : 'вне приватного участка')));
		return $res;
	}

	/**
	 * @param array $region
	 * @param int   $x
	 * @param int   $y
	 * @param int   $z
	 *
	 * @return bool
	 */
	public function inRegion($region, $x, $y, $z){
		if($x < $region["x"][0] || $x > $region["x"][1]) return false;
		if($z < $region["z"][0] || $z > $region["z"][1]) return false;
		if($y < $region["y"][0] || $y > $region["y"][1]) return false;
		return true;
	}

	public function overInRegion($region, $x, $y, $z){
		//Возвращает 0, если точка находится вне участка
		//1- если под участком
		//2- если над  участком
		//3- если внутри участка
		if($x < $region["x"][0] || $x > $region["x"][1]) return 0;
		if($z < $region["z"][0] || $z > $region["z"][1]) return 0;
		if($y < $region["y"][0]) return 1;
		if($y > $region["y"][1]) return 2;
		return 3;
	}

	public function intersectWithRegion($region, $x0, $y0, $z0, $x1, $y1, $z1){
		if($x0 > $x1){
			$x = $x1;
			$x1 = $x0;
			$x0 = $x;
		}
		$px = $region["x"];
		if($x1 < $px[0] || $x0 > $px[1]) return false;

		if($z0 > $z1){
			$z = $z1;
			$z1 = $z0;
			$z0 = $z;
		}
		$pz = $region["z"];
		if($z1 < $pz[0] || $z0 > $pz[1]) return false;

		if($y0 > $y1){
			$y = $y1;
			$y1 = $y0;
			$y0 = $y;
		}
		$py = $region["y"];
		if($y1 < $py[0] || $y0 > $py[1]) return false;
		return true;
	}

	public function getVolume($region){
		$px = $region["x"];
		$py = $region["y"];
		$pz = $region["z"];
		return abs(($px[1] - $px[0] + 1) * ($py[1] - $py[0] + 1) * ($pz[1] - $pz[0] + 1));
	}

	public function getVolumePP($pNameLC){
		$pos1 = $this->pos1[$pNameLC];
		$pos2 = $this->pos2[$pNameLC];
		$minX = min($pos1[0], $pos2[0]);
		$maxX = max($pos1[0], $pos2[0]);
		$minY = min($pos1[1], $pos2[1]);
		$maxY = max($pos1[1], $pos2[1]);
		$minZ = min($pos1[2], $pos2[2]);
		$maxZ = max($pos1[2], $pos2[2]);
		return ($maxX - $minX + 1) * ($maxY - $minY + 1) * ($maxZ - $minZ + 1);
	}

	/**
	 * Выделяет цветом участок текста (за выделенным будет цвет в соответствии с $ResultCode)
	 *
	 * @param string $txt   - выделяемый текст
	 * @param int    $ResultCode
	 * @param string $color цвет выделения в формате '§x' . По умолчанию: LIGHT_PURPLE
	 *
	 * @return string
	 */
	public function ec($txt, $ResultCode, $color = null){
		if($color == null) $color = '§d';
		$nextColor = $this->getColorByResultCode($ResultCode);
		return $color . $txt . $nextColor;
	}

	/**
	 * Форматирует координаты стандартным образом
	 *
	 * @param      $x
	 * @param      $y
	 * @param      $z
	 * @param bool $label
	 *
	 * @return string
	 */
	public function fCrd($x, $y, $z, $label = false){
		$ret = ($label ? '§dКоординаты ' : '');
		return $ret . "§7x:§6$x §7y:§6$y §7z:§6$z";
	}

	/**
	 * Форматирует команду стандартным образоm
	 *
	 * @param string     $cmd
	 * @param string|int $nextColor
	 *
	 * @return string
	 */
	public function frc($cmd, $nextColor){
		if(is_integer($nextColor)){
			$nextColor = $this->getColorByResultCode($nextColor);
		}
		return '§e' . $cmd . $nextColor . '';
	}

	/**
	 * Форматирует имя участка стандартным образом
	 *
	 * @param string     $regionName
	 * @param string|int $nextColor
	 *
	 * @return string
	 */
	public function frn($regionName, $nextColor){
		if(is_integer($nextColor)){
			$nextColor = $this->getColorByResultCode($nextColor);
		}
		return '"§6' . $regionName . $nextColor . '"';
	}

	/**
	 * Форматирует имя мира стандартным образом
	 *
	 * @param string     $levelName
	 * @param string|int $nextColor
	 *
	 * @return string
	 */
	public function frw($levelName, $nextColor){
		if(is_integer($nextColor)){
			$nextColor = $this->getColorByResultCode($nextColor);
		}
		return '"§7' . $levelName . $nextColor . '"';
	}

	private function updateGroup(Player $s, $args, $group, $action){

		if(!isset($args[1]) || !isset($args[2])){
			return $this->MSG($s, 0, "[✘]" . $this->getCmdUsage(strtolower($args[0]), 0, true));
		}

		$regionName = $args[1];
		$regionNameLC = strtolower($regionName);
		$pName = $args[2];
		$pNameLC = strtolower($pName);
		$regions = $this->areas->getAll();

		if(!isset($regions[$regionNameLC])){
			return $this->MSG($s, 0, "[✘] Участок \"§6" . $regionName . "§a\" не существует.");
		}
		$region = $regions[$regionNameLC];
		$yourNameLC = strtolower($s->getName());
		if(!in_array($yourNameLC, $region["owners"]) && !$s->hasPermission("rg.do-all")){
			return $this->MSG($s, 0, "[✘] У Вас недостаточно прав.");
		}
		$list = $region[$group];
		$bAdd = $action == "add";
		$bow = $group == 'owners';

		if($bAdd){
			$list[] = $pNameLC;
		}else{
			if($bow && $pNameLC == $yourNameLC){
				if($pNameLC == $yourNameLC) return $this->MSG($s, 0, "[✘] Плохая идея удалять себя из собственников участка.");
				if(count($list) < 2) return $this->MSG($s, 0, "[✘] Нельзя удалять последнего собственника участка.");
			}
			$key = array_search($pNameLC, $list);
			if($key === false) return $this->MSG($s, 0, '[✘] На участке ' . $this->frn($regionName, 0) . ' нет ' . ($bow ? 'собственника ' : 'жильца ') . '"§b' . $pName . '§a" ');
			array_splice($list, $key, 1);
		}
		$regions[$regionNameLC][$group] = $list;
		$this->areas->setAll($regions);
		$this->areas->save();
		return $this->MSG($s, 1, "[✔] " .
			($bAdd ? 'К участку ' : 'С участка ') .
			$this->frn($regionName, 1) .
			($bAdd ? ' добавлен ' : ' удален ') .
			($bow ? 'собственник ' : 'жилец ') .
			'"§b' . $pName . '§a" '
		);
	}

	/**
	 * @param $flagName
	 * @param $flagValue
	 * @param $regionName
	 *
	 * @return string
	 */
	private function SetFlag($flagName, $flagValue, $regionName){
		$regionNameLC = strtolower($regionName);
		$regions = $this->areas->getAll();
		$regions[$regionNameLC]["flags"][$flagName] = $flagValue;
		$this->areas->setAll($regions);
		$this->areas->save();
		$fValTxt = $this->flagValuesTxt[$flagValue];
		$msg = "[✔] На участке " . $this->frn($regionName, 1) . " флаг " . $this->ec($flagName, 1) . "§a установлен в " . $this->flagColors[$flagValue] . "$flagValue  ($fValTxt)";
		$msg .= "\n" . $this->getFlagHelpUsage($flagName, '§7');
		return $msg;

	}

	private function getFlagHelpUsage($flagName = "<флаг>", $startColor = '§7'){
		return $startColor . "Справка по флагу: §e/rg fi $flagName " . $startColor . " Справка по значениям флагов: §e/rg fi";
	}

	private function cancelScheduledEdgesTasks($regionNameLC){
		if(isset($this->scheduledEdgesTasks[$regionNameLC])){
			$this->scheduledEdgesTasks[$regionNameLC]->cancel();
			unset($this->scheduledEdgesTasks[$regionNameLC]);
		}
	}

	/**
	 * @param   string $regionName
	 * @param   Player $sender
	 * @param int      $timeToShowSec
	 *
	 * @return bool
	 */
	public function setRegionEdges($regionName, $sender, $timeToShowSec = 15){
		$regionNameLC = strtolower($regionName);
		$region = $this->areas->get($regionNameLC);

		$levelName = $region["level"];
		$level = $this->getServer()->getLevelByName($levelName);

		$direct = false;
		$update = false;

		$flatPairs = [[0, 0], [0, 1], [1, 0], [1, 1]];
		$axis3 = ["x", "y", "z"];
		$hashes = [];
		$edgesOldBlocks = [];
		for($axi = 0; $axi <= 2; $axi++){
			$vMin = $region[$axis3[$axi]][0];
			$vMax = $region[$axis3[$axi]][1];

			$arFlat = array_diff([0, 1, 2], [$axi]);
			sort($arFlat);
			foreach($flatPairs as $pair){
				for($v = $vMin; $v <= $vMax; $v++){
					$arv = [];
					$arv[$axi] = $v;
					foreach($arFlat as $k => $axi2){
						$arv[$axi2] = $region[$axis3[$axi2]][$pair[$k]];
					}
					$hash = $arv[0] . ',' . $arv[1] . ',' . $arv[2];
					if(($v != $vMin && $v != $vMin) || !in_array($hash, $hashes)){
						$arv[3] = $v % 2 ? self::EDGE_BLOCK_ID_1 : self::EDGE_BLOCK_ID_2;
						$arv[4] = $level->getBlockIdAt($arv[0], $arv[1], $arv[2]);
						$edgesOldBlocks[] = $arv;
						$hashes[] = $hash;
					}
				}
			}
		}

		$failMsg = "[✘] При попытке отобразить границы участка " . $this->frn($regionName, 0) . " произошла ошибка.";
		$deleteTime = intval(microtime(true) + $timeToShowSec);
		$eRegion = [
			"level"          => $levelName,
			'deleteTime'     => $deleteTime,
			'deleteTimeH'    => date('Y.m.d H:i:s', $deleteTime),
			'hashes'         => $hashes,
			"edgesOldBlocks" => $edgesOldBlocks
		];
		$this->edges->set($regionNameLC, $eRegion);
		if(!$this->edges->save()){
			$this->getLogger()->info(F::RED . "Произошла ошибка при попытке сохранить areas.yml (PMC_Rg, fn " . __FUNCTION__ . ")");
			return $this->MSG($sender, 0, $failMsg, false);
		}

		$blocksSetCount = 0;
		foreach($edgesOldBlocks as $OldBlock){
			$pos = new Position($OldBlock[0], $OldBlock[1], $OldBlock[2], $level);
			$block = new Block($OldBlock[3], 34567);
			if($level->setBlock($pos, $block, $direct, $update)){
				$blocksSetCount++;
			}
		}
		if($blocksSetCount < count($edgesOldBlocks)){
			$this->getLogger()->info(F::RED . "При установке границ участка $regionName некоторые блоки не удалось установить " . __FUNCTION__ . ")");
			return $this->MSG($sender, 0, $failMsg, false);
		}

		$this->cancelScheduledEdgesTasks($regionNameLC);

		$this->scheduledEdgesTasks[$regionNameLC] = $this->getServer()->getScheduler()->scheduleDelayedTask(new Tasks\DeleteEdgesTask($this, $sender, $regionName), 20 * $timeToShowSec);
		return $this->MSG($sender, 1, "[✔] Показаны границы участка " . $this->frn($regionName, 1) . " на $timeToShowSec секунд.");
	}

	/**
	 * @param string      $regionName
	 * @param Player|null $sender
	 *
	 * @return bool
	 */
	public function deleteEdges($regionName, $sender = null){

		$regionNameLC = strtolower($regionName);
		$eRegion = $this->edges->get($regionNameLC);
		if(!$eRegion){
			$this->getLogger()->warning(F::RED . "Не найдены границы участка '$regionNameLC' в Edges.yml при попытке их удаления.");
			return true;
		}

		$levelName = $eRegion["level"];
		$level = $this->getServer()->getLevelByName($levelName);
		if($level == null){
			$this->getLogger()->error("Не найден мир '$levelName' при попытке удаления границы участка '$regionNameLC'.");
			$this->edges->remove($regionNameLC);
			$this->edges->save();
			return false;
		}

		$this->cancelScheduledEdgesTasks($regionNameLC);

		$direct = false;
		$update = false;
		if(isset($eRegion['edgesOldBlocks'])){
			$edgesOldBlocks = $eRegion['edgesOldBlocks'];
			foreach($edgesOldBlocks as $OldBlock){
				$pos = new Position($OldBlock[0], $OldBlock[1], $OldBlock[2], $level);
				if($level->getBlockIdAt($OldBlock[0], $OldBlock[1], $OldBlock[2]) == $OldBlock[3]){
					$level->setBlock($pos, new Block($OldBlock[4]), $direct, $update);
				}
			}
		}

		$this->edges->remove($regionNameLC);
		$this->edges->save();
		if($sender !== null){
			$this->MSG($sender, 1, "[✔] Границы участка " . $this->frn($regionName, 1) . " скрыты.");
		}
		return true;
	}

	/**
	 * @param string $regionName
	 * @param Player $sender
	 */
	public function forceDeleteRegionEdges($regionName, $sender){
		$regionNameLC = strtolower($regionName);
		$region = $this->areas->get($regionNameLC);

		$level = $this->getServer()->getLevelByName($region["level"]);

		$this->cancelScheduledEdgesTasks($regionNameLC);
		$direct = false;
		$update = false;

		$flatPairs = [[0, 0], [0, 1], [1, 0], [1, 1]];
		$axis3 = ["x", "y", "z"];
		$arv = [];
		for($axi = 0; $axi <= 2; $axi++){
			$vMin = $region[$axis3[$axi]][0];
			$vMax = $region[$axis3[$axi]][1];

			$arFlat = array_diff([0, 1, 2], [$axi]);
			sort($arFlat);
			foreach($flatPairs as $pair){
				for($v = $vMin; $v <= $vMax; $v++){
					$arv[$axi] = $v;
					foreach($arFlat as $k => $axi2){
						$arv[$axi2] = $region[$axis3[$axi2]][$pair[$k]];
					}
					$pos = new Position($arv[0], $arv[1], $arv[2], $level);
					$level->setBlock($pos, new Air(), $direct, $update);
				}
			}
		}
		$this->MSG($sender, 1, "[✔] Очищены границы участка: " . $this->frn($regionName, 1));
	}

	/**
	 * @param string   $pName
	 * @param Position $pos
	 * @param string   $flagName
	 * @param int|null $forceFlagValue
	 *
	 * @return bool
	 */
	public function checkFlag($pName, $pos, $flagName, $forceFlagValue = null){
		$pNameLC = strtolower($pName);
		$x = $pos->getFloorX();
		$y = $pos->getFloorY();
		$z = $pos->getFloorZ();
		$levelName = $pos->getLevel()->getName();
		foreach($this->areas->getAll() as $regionName => $region){
			if($levelName == $region["level"] && $this->inRegion($region, $x, $y, $z)){
				return $this->checkFlagForRegion($pNameLC, $region, $flagName, $forceFlagValue);
			}
		}
		return true;
	}

	public function checkFlagForRegion($pNameLC, $region, $flagName, $forceFlagValue = null){
		$flagValue = ($forceFlagValue != null) ? $forceFlagValue : $this->flagValues[$region["flags"][$flagName]];
		if(!$pNameLC) return $flagValue >= 3; //Если ник не передан, то разрешено, если "для всех".
		if($flagValue < 1) return false; //== 0. Запрещено для всех
		if($flagValue > 2) return true; //== 3 Разрешено для всех. Остаются значения флага 1 и 2
		if(in_array($pNameLC, $region["owners"])) return true; //Если собственник при значении > 0, то разрешено
		if($flagValue == 1) return false; //Если не собственник, то при значении 1 - запрещено. Остается значение 2
		return in_array($pNameLC, $region["members"]); //При 2 разрешено жителям (собственник ушли выше).
	}


	/**
	 * Проверка права изменения (указанного) флага (указанного) участка (указанным) плеером
	 *
	 * @param CS     $s Плеер-сендер
	 * @param array  $region
	 * @param string $flagName
	 *
	 * @return bool
	 */
	function checkChangeFlagRights(CS $s, $region, $flagName){
		$pNameLC = strtolower($s->getName());
		if($s->hasPermission("rg.do-all")) return true;
		if(!$s->hasPermission("rg.command.flag")) return false;
		if(!$s->hasPermission("change.flag.$flagName")) return false;
		if(!in_array($pNameLC, $region["owners"])) return false;
		return true;
	}

	function canUseRGCommand(CS $s, $subCmd){
		if($s->hasPermission("rg.do-all")) return true;
		if($s->hasPermission("rg.command.$subCmd")) return true;
		return false;
	}

	function reloadAndRepairAreas(){
		$this->areas->reload();
		$this->edges->reload();
		$areas = $this->areas->getAll();
		$DefaultFlags = $this->config->getAll()['DefaultFlags'];
		$bSave = false;
		foreach($areas as $regionName => $region){
			$areaFlags = $region['flags'];
			foreach($areaFlags as $flag => $value){
				if(!array_key_exists($flag, $DefaultFlags)){
					unset($areaFlags[$flag]);
					$bSave = true;
				}
			}
			foreach($DefaultFlags as $flag => $value){
				if(!array_key_exists($flag, $areaFlags)){
					$areaFlags[$flag] = $value;
					$bSave = true;
				}
			}
			if($bSave){
				$areas[$regionName]['flags'] = $areaFlags;
			}
		}
		if($bSave){
			$this->areas->setAll($areas);
			$this->areas->save();
			$this->areas->reload();
		}
	}


	public function s_rtrim($str,$toCut){
		$pattern = preg_replace('/\./', '\.', $toCut);
		return preg_replace('~' . $pattern . '$~i', '', $str);
	}
	
	function onDisable(){
		$this->getLogger()->info(F::RED . "Приват система выключена");
		$this->areas->save();
	}
}
