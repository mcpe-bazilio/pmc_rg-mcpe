<?php
namespace PMC_Rg;

use pocketmine\block\Block;
use pocketmine\command\CommandSender as CS;
use pocketmine\event\block\BlockBreakEvent;
use pocketmine\event\block\BlockPlaceEvent;
use pocketmine\event\entity\EntityCombustEvent;
use pocketmine\event\entity\EntityDamageByEntityEvent;
use pocketmine\event\entity\EntityDamageEvent;
use pocketmine\event\entity\EntityExplodeEvent;
use pocketmine\event\entity\EntityRegainHealthEvent;
use pocketmine\event\entity\EntityTeleportEvent;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerBedEnterEvent;
use pocketmine\event\player\PlayerBucketEmptyEvent;
use pocketmine\event\player\PlayerBucketFillEvent;
use pocketmine\event\player\PlayerChatEvent;
use pocketmine\event\player\PlayerCommandPreprocessEvent;
use pocketmine\event\player\PlayerDropItemEvent;
use pocketmine\event\player\PlayerInteractEvent;
use pocketmine\event\player\PlayerMoveEvent;
use pocketmine\event\player\PlayerQuitEvent;
use pocketmine\Player;


class EventListener implements Listener {

	private $plugin;
	private $playersInfo = [];

	const MAX_BREAK_EDGE_COUNT_BEFORE_ALERT = 20;

	function __construct(PMC_Rg $plugin){
		$this->plugin = $plugin;
	}

	private function MSG(CS $s, $msgType, $msg, $ret = null){
		return $this->plugin->MSG($s, $msgType, $msg, $ret);
	}

	/**
	 * Проверка возможности выполнения действия. Если плеер передан, то будут
	 * проверяться его ($person) права на действие ($action) на участке,
	 * где находится предмет действия (определяется $pos)
	 *
	 * @param Player                             $person
	 * @param Block | \pocketmine\level\Position $pos
	 * @param \pocketmine\event\Event            $e
	 * @param string                             $flagName
	 * @param string                             $failMsg
	 *
	 * @return bool
	 */
	private function cr($person, $pos, $e, $flagName, $failMsg = null){
		if($person == null){
			$pName = '';
		}else{
			if(!$person instanceof Player) return true;
			if($person->hasPermission("rg.do-all")) return true;
			$pName = $person->getName();
		}

		if($pos == null) $pos = $person->getPosition();
		if($this->plugin->checkFlag($pName, $pos, $flagName)) return true;
		$e->setCancelled();
		if($failMsg != null) $this->MSG($person, 0, $failMsg);
		return false;
	}

	function useCommands(PlayerCommandPreprocessEvent $e){
		$this->cr($e->getPlayer(), null, $e, "cmd", "[✘] Использовать команды тут запрещено. Приватная территория!");
	}

	//TODO проверить
	function bucketEmpty(PlayerBucketEmptyEvent $e){
		$this->cr($e->getPlayer(), null, $e, "bucket", "[✘] Использовать ведро тут запрещено. Приватная территория!");
	}

	//TODO проверить
	function bucketFillEvent(PlayerBucketFillEvent $e){
		$this->cr($e->getPlayer(), null, $e, "bucket", "[✘] Использовать ведро тут запрещено. Приватная территория!");
	}

	//TODO проверить
	function ItemDrop(PlayerDropItemEvent $e){
		$this->cr($e->getPlayer(), null, $e, "drop", "[✘] Выбрасывать предметы тут запрещено. Приватная территория!");
	}

	//TODO проверить
	function noBed(PlayerBedEnterEvent $e){
		$this->cr($e->getPlayer(), null, $e, "sleep", "[✘] Спать тут запрещено. Приватная территория!");
	}

	/**
	 * @param EntityDamageEvent|EntityDamageByEntityEvent $e
	 */
	function EntityDE(EntityDamageEvent $e){
		if($e instanceof EntityDamageByEntityEvent){
			$victim = $e->getEntity();
			if(!$victim instanceof Player) return;
			$this->cr($e->getDamager(), $victim->getPosition(), $e, "pvp", "[✘] Атаковать здесь запрещено. Приватная территория!");
		}elseif($e instanceof EntityDamageEvent){
			$this->cr(null, $e->getEntity(), $e, "god");
		}
	}

	function EntityExplode(EntityExplodeEvent $e){
		$this->cr(null, $e->getEntity(), $e, "explode");
	}

	function EntityCombust(EntityCombustEvent $e){
		$this->cr(null, $e->getEntity(), $e, "burn");
	}

	function EntityRegain(EntityRegainHealthEvent $e){
		$this->cr(null, $e->getEntity(), $e, "regain");
	}

	function EntityTeleport(EntityTeleportEvent $e){
		$ok = $this->cr($e->getEntity(), $e->getFrom(), $e, "teleport", "[✘] Телепортация отсюда запрещена. Приватная территория!");
		if(!$ok) return;
		$this->cr($e->getEntity(), $e->getTo(), $e, "entry", "[✘] Телепортация туда запрещена. Там приватная территория!");
	}

	function PlayerChat(PlayerChatEvent $e){
		$this->cr($e->getPlayer(), null, $e, "chat", "[✘] Чатиться тут запрещено. Приватная территория!");
	}

	function PlayerMove(PlayerMoveEvent $e){
		$this->cr($e->getPlayer(), null, $e, "entry", "[✘] Входить сюда запрещено. Приватная территория!");
	}

	function PlayerInteractEvent(PlayerInteractEvent $e){
		$this->checkEdgeAndRights($e->getPlayer(), $e->getBlock(), $e, "use", "[✘] Действия тут запрещены. Приватная территория!");
	}

	function BlockPlaceEvent(BlockPlaceEvent $e){
		$this->checkEdgeAndRights($e->getPlayer(), $e->getBlock(), $e, "build", "[✘] Ставить блоки здесь запрещено. Приватная территория!");
	}

	function BlockBreakEvent(BlockBreakEvent $e){
		$this->checkEdgeAndRights($e->getPlayer(), $e->getBlock(), $e, "build", "[✘] Разрушать блоки здесь запрещено. Приватная территория!");
	}

	/**
	 * @param Player                             $person
	 * @param Block | \pocketmine\level\Position $pos
	 * @param \pocketmine\event\Event            $e
	 * @param string                             $flagName
	 * @param string|null                        $failMsg
	 *
	 * @return bool
	 */
	function checkEdgeAndRights($person, $pos, $e, $flagName, $failMsg = null){
		if(!$person instanceof Player) return true;
		//if($person->hasPermission("rg.do-all")) return true; //TODO Подумать, куда правильнее воткнуть это
		$pName = $person->getName();
		$pNameLC = strtolower($pName);
		$x = $pos->getFloorX();
		$y = $pos->getFloorY();
		$z = $pos->getFloorZ();

		$levelName = $pos->getLevel()->getName();

		foreach($this->plugin->areas->getAll() as $regionName => $region){
			if($levelName == $region["level"] && $this->plugin->inRegion($region, $x, $y, $z)){
				//Если позиция действия на участке и для него показаны границы,
				//проверим, если позиция действия находится на границе, то отменим
				$eRegions = $this->plugin->edges->getAll();
				if(isset($eRegions[$regionName])){
					if(in_array($x . ',' . $y . ',' . $z, $eRegions[$regionName]['hashes'])){
						if($pNameLC != '' && $e->getEventName() == 'pocketmine\event\block\BlockBreakEvent'){
							if(!isset($this->playersInfo[$pNameLC])){
								$this->playersInfo[$pNameLC]['breakCount'] = 1;
							}else{
								$this->playersInfo[$pNameLC]['breakCount'] += 1;
							}
							if($this->playersInfo[$pNameLC]['breakCount'] > self::MAX_BREAK_EDGE_COUNT_BEFORE_ALERT){
								$this->plugin->MSG($person, 3, "Эй, $pName, хватит уже ломать границы участка! Бросай это бесполезное дело! :)");
								$this->playersInfo[$pNameLC]['breakCount'] = 0;
							}
						}
						$e->setCancelled();
						return false;
					}
				}
				if($person->hasPermission("rg.do-all")) return true;
				if($this->plugin->checkFlagForRegion($pNameLC, $region, $flagName)) return true;
				$e->setCancelled();
				if($failMsg != null) $this->plugin->MSG($person, 0, $failMsg);
				return false;
			}
		}
		return true;
	}

	function PlayerQuit(PlayerQuitEvent $e){
		$pNameLC = strtolower($e->getPlayer()->getName());
		$p = $this->plugin;
		if(isset($p->pos1[$pNameLC])) unset($p->pos1[$pNameLC]);
		if(isset($p->pos2[$pNameLC])) unset($p->pos2[$pNameLC]);
	}


}
